MKOS README!

This operating system is referred to as Marshall Kobey OS.

Currently this is "All Rights Reserved" as I am still working out licensing. Everything is Copyright (c) Marshall King.
You may copy and modify these files for development purposes. You may have your own copy on your computer. You may also copy this and give it to other people.



To build:
You will need a GCC cross compiler targetting "i686-elf". You will also need GNU binutils targetting "i686-elf" as well. Please make sure that your binutils and GCC are compatable versions.
Currently (28/4/15) only the intel i386/i686 architecture is supported. 
GCC 4.8.2 is the "official" compiler version, with binutils version 2.24. Both binutils and GCC need to be compiled "--without-headers".
You will also need GNU Make. It can be the one that your distribution uses, or it can be from mingw or cygwin. You may also compile it yourself. 
Your native GNU Make is the only supported version of GNU Make. On Windows, this means "mingw32-make". NOTE: "mingw32-make" IS NOT the same as "make" from MSYS or Cygwin.

Next you will need to "CD" into the kernel, and libc directorys, and type "make" or what ever the command to invoke GNU Make on your system is.
After that, in "kernel" you will have "myos.bin", and in libc, you will have many *.o files (both in the root and in sub-directorys) as well as some *.a files in the root of that folder.
Copy the *.a files to sysroot/System/Libraries, and copy myos.bin to sysroot/System. After myos.bin has been copies to System, it will need to be renamed to "System" (in order to have valid directory structure).
After that, you may continue! 
BIG NOTE: You do _NOT_ have to always copy the files over, just if you need to add a library to the compilers search path, or if you need to create a valid bootable "System" folder.
