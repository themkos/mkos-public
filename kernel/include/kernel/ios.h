// HeaderFile for ios.c
// Copyright (c) Marshall King
#ifndef _KERNEL_IOS_H
#define _KERNEL_IOS_H
static inline void outb(uint16_t port, uint8_t val);
static inline uint8_t inb(uint16_t port);
static inline void io_wait(void);
static inline unsigned long read_cr0(void);

#endif